export default {
  framerate: 30,
  cellSize: 10,
  colors: {
    alive: '#FFD1B6',
    dead: '#F9F9F9'
  },
  get(key, config) {
    if (typeof config === 'object' && config[key]) {
      return config[key];
    }

    return this[key];
  }
}

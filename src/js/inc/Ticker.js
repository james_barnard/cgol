'use strict'

export default class Ticker {

  constructor() {
    this.framerate = 30;
    this.paused = false;
  }

  listen(callback) {
    setTimeout(() => {
      callback(this);
      this.listen(callback);
    }, parseInt(1000 / this.framerate));
  }

}

'use strict';

var gulp = require('gulp');
var sass = require('gulp-sass');
var shell = require('gulp-shell');

var config = {
  scssSrc: './src/scss/**/*.scss',
  scssDest: './dist',
  jsEntry: 'src/js/main.js',
  jsDest: 'dist/'
};

gulp.task('webpack', function() {
  return gulp.src('*.js', {read: false})
    .pipe(shell([
      'webpack --progress --colors'
    ]))
});

gulp.task('sass', function () {
  return gulp.src(config.scssSrc)
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest(config.scssDest));
});

gulp.task('watch', function () {
  gulp.watch(config.scssSrc, ['sass']);
  gulp.watch('./src/js/**/*.js', ['webpack']);
  gulp.watch('./src/js/**/*.vue', ['webpack']);
});

'use strict'

import Vue from 'vue';
import VueResource from 'vue-resource';
import App from './components/app.vue';

Vue.use(VueResource);

Vue.component('App', App);

new Vue({
  el: '#app',
  template: '<App></App>'
});

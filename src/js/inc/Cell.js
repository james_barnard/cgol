'use strict'

import gConfig from '../config.js';

export default class Cell {
  constructor(config) {
    this.x = config.x;
    this.y = config.y;
    this.alive = false;

    this.size = gConfig.get('cellSize', {});
  }

  setNeighbours(neighbours) {
    this.neighbours = neighbours;
  }

  drawColor(stage) {
    let color = gConfig.colors.dead;

    if (this.alive) {
      color = gConfig.colors.alive;
    }

    stage.fillStyle = color;
  }

  draw(stage) {
    this.drawColor(stage);
    stage.fillRect(this.x, this.y, this.size, this.size);
  }

  awaken() {
    this.alive = true;
  }

  kill() {
    this.alive = false;
  }

  shouldKill() {
    let neighbours = this.neighbours;
    let numberAlive = 0;
    for (let neighbourDirection in neighbours) {
      if (neighbours.hasOwnProperty(neighbourDirection)) {
        let neighbour = neighbours[neighbourDirection];

        if (neighbour !== undefined && neighbour.isAlive()) {
          numberAlive++;
        }
      }
    }

    if (this.isAlive()) {
      // Any live cell with fewer than two live neighbours dies, as if caused by underpopulation.
      if (numberAlive < 2) {
        return true;
      }
      // Any live cell with two or three live neighbours lives on to the next generation.
      else if (numberAlive < 4) {
        return false;
      }
      // Any live cell with more than three live neighbours dies, as if by overpopulation.
      else {
        return true;
      }
    }
    // Any dead cell with exactly three live neighbours becomes a live cell, as if by reproduction.
    else if (numberAlive === 3) {
      return false;
    }

    return true;
  }

  isDead() {
    return !this.alive;
  }

  isAlive() {
    return !this.isDead();
  }
}

'use strict'

import Cell from './Cell.js';
import Ticker from './Ticker.js';
import gConfig from '../config.js';

export default class Stage {

  constructor(config) {
    this.config = config;

    // Get the canvas either as the actual element (el), or by id
    this.canvas = config.el ? config.el : document.getElementById(config.id);

    // Get the dimensions and apply to the canvas element
    this.height = gConfig.get('height', this.config);
    this.width = gConfig.get('width', this.config);
    this.canvas.width = this.width;
    this.canvas.height = this.height;
  }

  /**
   * Actually initialise the stage and canvas
   */
  init(starters) {
    let _this = this;
    let cellSize = gConfig.get('cellSize');

    _this.stage = _this.canvas.getContext('2d');

    // Set the refresh rate of the canvas
    _this.framerate = gConfig.get('framerate', _this.config);

    // Instantiate empty cache for the cells
    // This will be a grid e.g [x][y] = Cell
    _this.cellCache = [];

    // Instantiate empty cache for the potential change cells
    // (alive cells and their neighbours)
    _this.cellPotentialCache = [];

    // Build the world, first by initialising the cellCache X coords
    for (let i = 0; i < _this.width; i += cellSize) {
      _this.cellCache[i] = [];
    }

    let startersLength = starters.length;
    let startersFound = 0;
    let starterCells = [];

    // Then loop the cache and create and add cells
    _this.loopGridCache((x, y) => {
      let cell = new Cell({
        x: x,
        y: y
      });

      if (startersFound < startersLength) {
        starters.forEach((starter) => {
          // Convert x & y of starter coords to adjusted for cell size
          let starterX = starter.x * cellSize;
          let starterY = starter.y * cellSize;
          if (cell.x === starterX && cell.y === starterY) {
            startersFound++;
            cell.awaken();
          }
        });
      }

      if (cell.isAlive()) {
        starterCells.push(cell);
      }

      // Draw the cell to the stage
      cell.draw(_this.stage);
      _this.cellCache[cell.x][cell.y] = cell;
    });

    _this.loopGridCache((x, y) => {
      let cell = _this.cellCache[x][y];

      if (typeof cell === 'object' && cell instanceof Cell) {
        cell.setNeighbours(this.getNeighboursOf(cell));
      }
    });

    // We have all the alive starter cells, loop and add into the potential cells
    _this.buildPotentialCache(starterCells);

    // Start the iterations
    _this.initTicker();
  }

  initTicker() {
    let _this = this;

    _this.ticker = new Ticker;
    _this.ticker.framerate = _this.framerate;
    _this.ticker.paused = gConfig.get('paused', _this.config);
    _this.ticker.listen((ticker) => {
      if (ticker.paused) {
        return;
      }

      // Each tick we want to loop over the cells in existence
      // We check them against the rules and then destroy them if needed
      // We keep track of the state

      let cellAlterations = [];
      let newPotential = [];

      _this.cellPotentialCache.forEach(cell => {
        let alive = false;

        if (cell.shouldKill()) {
          if (cell.isAlive()) {
            cellAlterations.push({
              change: 'kill',
              cell: cell
            });
          }
        } else if (cell.isDead()) {
          cellAlterations.push({
            change: 'awaken',
            cell: cell
          });

          alive = true;
        } else {
          alive = true;
        }

        if (alive) {
          newPotential.push(cell);
        }
      });

      _this.buildPotentialCache(newPotential);

      cellAlterations.forEach((cellAlteration) => {
        if (cellAlteration.change === 'awaken') {
          cellAlteration.cell.awaken();
        } else if (cellAlteration.change === 'kill') {
          cellAlteration.cell.kill();
        }

        // Draw the altered cell to the stage
        cellAlteration.cell.draw(_this.stage);
      });
    });
  }

  buildPotentialCache(aliveCells) {
    let _this = this;

    // Start from scratch
    _this.cellPotentialCache = [];

    // Cache to check we don't store duplicates
    let dupeGridCheck = [];

    // Potential cache is the alive cells and their direct neighbours
    aliveCells.forEach(cell => {
      if (dupeGridCheck[cell.x] && dupeGridCheck[cell.x][cell.y]) {
        return;
      } else if (!dupeGridCheck[cell.x]) {
        dupeGridCheck[cell.x] = [];
      }

      dupeGridCheck[cell.x][cell.y] = true;

      _this.cellPotentialCache.push(cell);
      let neighbours = _this.getNeighboursOf(cell);

      // Add neighbours in too
      for (let neighbourDir in neighbours) {
        if (
          neighbours.hasOwnProperty(neighbourDir) &&
          typeof neighbours[neighbourDir] === 'object' &&
          (
            !dupeGridCheck[neighbours[neighbourDir].x] ||
            !dupeGridCheck[neighbours[neighbourDir].x][neighbours[neighbourDir].y]
          )
        ) {
          _this.cellPotentialCache.push(neighbours[neighbourDir]);
        }
      }
    });
  }

  /**
   * Get the N,E,S,W,NE,NW,SW,SE neighbours for the specified cell
   */
  getNeighboursOf(cell) {
    let x = cell.x;
    let y = cell.y;
    let cellSize = gConfig.get('cellSize');

    return {
      n: this.cellCache[x] ? this.cellCache[x][y - cellSize] : undefined,
      w: this.cellCache[x - cellSize] ? this.cellCache[x - cellSize][y] : undefined,
      e: this.cellCache[x + cellSize] ? this.cellCache[x + cellSize][y] : undefined,
      s: this.cellCache[x] ? this.cellCache[x][y + cellSize] : undefined,
      nw: this.cellCache[x - cellSize] ? this.cellCache[x - cellSize][y - cellSize] : undefined,
      ne: this.cellCache[x + cellSize] ? this.cellCache[x + cellSize][y - cellSize] : undefined,
      sw: this.cellCache[x - cellSize] ? this.cellCache[x - cellSize][y + cellSize] : undefined,
      se: this.cellCache[x + cellSize] ? this.cellCache[x + cellSize][y + cellSize] : undefined
    };
  }

  /**
   * Loop over the cached grid (x / y), calling the passed callback for every
   * cell.
   */
  loopGridCache(callback) {
    let cellSize = gConfig.get('cellSize');
    for (let i = 0; i < this.width; i += cellSize) {
      for (let j = 0; j < this.height; j += cellSize) {
        if (typeof callback === 'function') {
          callback(i, j);
        }
      }
    }
  }

  pause() {
    this.ticker.paused = true;
  }

  start() {
    this.ticker.paused = false;
  }

  updateFramerate(framerate) {
    this.ticker.framerate = framerate;
  }
}
